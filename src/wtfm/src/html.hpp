#pragma once

#include "config.hpp"

#include <ostream>

namespace wtfm {
	class html_page {
	public:
		class definition_table {
		public:
			definition_table(std::ostream& os, const std::string& defining_header);
			~definition_table();

			auto add_entry(const std::string& definition, const std::string& since) -> definition_table&;

		private:
			std::ostream& _os;
		};

		class member_table {
		public:
			member_table(std::ostream& os, const std::string& name);
			~member_table();

			auto add_entry(const std::string& name, const std::string& brief) -> member_table&;

		private:
			std::ostream& _os;
		};

		html_page(const config& config, std::ostream& os, const std::string& name);
		~html_page();
		auto add_heading(const std::string& name) -> html_page&;
		auto add_definition_table(const std::string& defining_header) {
			return definition_table(_os, defining_header);
		}
		auto add_member_table(const std::string& name) {
			return member_table(_os, name);
		}

	private:
		const config* _config;
		std::ostream& _os;
	};
}