#pragma once

#include "config.hpp"
#include "util.hpp"

#include <string>

#include <cppast/cpp_friend.hpp>

namespace wtfm {
	class friend_descriptor {
	public:
		friend_descriptor(const config& config, const cppast::cpp_friend& entity) : _config(&config), _entity(&entity) {}

		auto is_type() const {
			return !_entity->entity().has_value();
		}

		auto type() const {
			if (_entity->type().has_value()) {
				return cppast::to_string(_entity->type().value());
			}
			throw std::runtime_error("Friend declaration without a type.");
		}

		auto& entity() const {
			if (_entity->entity().has_value()) {
				return _entity->entity().value();
			}
			throw std::runtime_error("Friend declaration without a coresponding cursor.");
		}

		auto name() const {
			return is_type() ? type() : _entity->entity().value().name();
		}

		auto full_qualified_name() const {
			return is_type() ? "" : wtfm::full_qualified_name(_entity->entity().value());
		}

	private:
        const config*             _config [[maybe_unused]];
		const cppast::cpp_friend* _entity;
	};
}
