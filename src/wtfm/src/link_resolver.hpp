#pragma once

#include "config.hpp"

#include <string>
#include <unordered_map>

namespace wtfm {
	class link_resolver {
	public:
		static auto resolve(const config& config, const std::string& full_qualified_name, const std::string&)
		        -> std::string;
		static auto resolve(const config& config, const std::string& full_qualified_name) -> std::string;

	private:
		static auto& _links() {
			static std::unordered_map<std::string, std::string> links;
			return links;
		}
	};
}