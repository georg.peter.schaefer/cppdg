#include "enum_descriptor.hpp"

#include "html.hpp"
#include "util.hpp"

#include <string>

#include <boost/spirit/home/x3.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	namespace detail {
		const boost::spirit::x3::rule<struct enum_comment_class, wtfm::enum_descriptor::comment_t> enum_comment =
		        "enum_comment";
		auto enum_comment_def = rules::paragraph >> rules::paragraph;
		BOOST_SPIRIT_DEFINE(enum_comment)
	}

	enum_descriptor::enum_descriptor(const wtfm::config& config, const cppast::cpp_enum& entity)
	    : _config(&config), _entity(&entity) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.cbegin() + 1,
			                                comment_string.cend() - 1,
			                                detail::enum_comment,
			                                boost::spirit::x3::space,
			                                _comment);
		});
	}

	auto enum_descriptor::generate() const -> void {
		auto full_qualified_name = wtfm::full_qualified_name(*_entity);
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;

		mstch::array description{};
		for (auto&& [name, enum_value_descriptor] : _values) {
			description.emplace_back(mstch::map{{"name", name}, {"brief", enum_value_descriptor.comment().detailed}});
		}
		mstch::array constants{mstch::map{
		        {"description_heading", std::string("Constants")},
		        {"sub_heading", mstch::map{{"first", std::string("Constant")}, {"second", std::string("Meaning")}}},
		        {"description", description}}};

		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("template.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"defining_header", defining_header()},
                       {"definitions", mstch::array{mstch::map{{"definition", definition()}}}},
                       {"brief", _comment.brief},
                       {"detailed", _comment.detailed},
                       {"descriptions", constants}};

		os << mstch::render(page, context) << std::endl;
	}

	auto enum_descriptor::definition() const -> std::string {
		std::string definition;

		definition += "enum ";
		definition += (_entity->is_scoped() ? "class " : "");
		definition += _entity->name() + " {\n";
		std::size_t count = 0;
		for (auto&& [name, enum_value_descriptor] : _values) {
			++count;
			definition += indent + name + " = " + enum_value_descriptor.value();
			if (count < _values.size()) {
				definition += ",";
			}
			definition += "\n";
		}
		definition += "};";

		return definition;
	}
}
