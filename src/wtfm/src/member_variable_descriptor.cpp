#include "member_variable_descriptor.hpp"

#include "util.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/spirit/home/x3.hpp>

#include <cppast/cpp_storage_class_specifiers.hpp>
#include <cppast/cpp_type.hpp>

#include <mstch/mstch.hpp>

namespace wtfm {
	namespace detail {

		const boost::spirit::x3::rule<struct member_variable_comment_class, wtfm::member_variable_descriptor::comment_t>
		        member_variable_comment     = "member_variable_comment";
		auto    member_variable_comment_def = rules::paragraph >> rules::paragraph;
		BOOST_SPIRIT_DEFINE(member_variable_comment)
	}

	member_variable_descriptor::member_variable_descriptor(const wtfm::config&               config,
	                                                       const cppast::cpp_member_variable& entity)
	    : _config(&config), _entity(&entity) {
		entity.comment().map([this](auto&& comment_string) {
			boost::spirit::x3::phrase_parse(comment_string.cbegin() + 1,
			                                comment_string.cend() - 1,
			                                detail::member_variable_comment,
			                                boost::spirit::x3::space,
			                                _comment);
		});
	}

	auto member_variable_descriptor::generate() const -> void {
		auto full_qualified_name = wtfm::full_qualified_name(*_entity);
		auto filename            = full_qualified_name + ".html";
		auto path                = _config->output / filename;
		auto parent_name_length  = full_qualified_name.rfind("::");
		auto parent_name =
		        parent_name_length != std::string::npos ? full_qualified_name.substr(0, parent_name_length + 2) : "";
		auto entity_name = parent_name_length != std::string::npos ? full_qualified_name.substr(parent_name_length + 2)
		                                                           : full_qualified_name;

		std::ofstream os(path.native(), std::ofstream::trunc);
		std::ifstream is("member_variable.mustache");
		std::string   page((std::istreambuf_iterator<char>(is)), std::istreambuf_iterator<char>());
		mstch::map    context{{"project_name", _config->name},
                       {"scope", full_qualified_name},
                       {"parent_scope", parent_name},
                       {"name", entity_name},
                       {"definitions", mstch::array{mstch::map{{"definition", definition()}}}},
                       {"brief", _comment.brief},
                       {"detailed", _comment.detailed}};

		os << mstch::render(page, context) << std::endl;
	}

	auto member_variable_descriptor::definition() const -> std::string {
		std::string definition;

		auto type = cppast::to_string(_entity->type());
		if (auto range = boost::find_first(type, " const"); range) {
			boost::erase_all(type, range);
			type = "const " + type;
		}

		definition += type + " " + _entity->name();

		if (_entity->is_mutable()) {
			definition = "mutable " + definition;
		}

		auto default_value = _entity->default_value();
		if (default_value.has_value()) {
			definition += " = " + wtfm::expression(default_value.value());
		}

		definition += ";";

		return definition;
	}
}
